/*
 * enums.h
 *
 *  Created on: 30 de mar. de 2016
 *      Author: ATR
 */

#ifndef ENUMS_H_
#define ENUMS_H_

//Pantallas
#define RT_VIEW_1 0
#define MENU_VIEW 1

//modo
#define OFF 0
#define TABLE_TURBO 1
#define TABLE_ENGINE 2
#define TEST 3
#define FIXED_GEOM 4

#endif /* ENUMS_H_ */
