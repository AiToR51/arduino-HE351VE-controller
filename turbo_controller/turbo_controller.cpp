#include <Arduino.h>
#include <SPI.h>
//#include "CAN/CAN.h"
#include "CAN2/mcp_can.h"
#include "utils.c"
#include "LCDKeypad/LCDKeypad.h"
#include "enums.h"

#define ENABLE_DEBUG false
#define ENABLE_SERIAL_SENDING_INFO true
#define TABLE_X_AXIS_LENGTH 8
#define TABLE_Y_AXIS_LENGTH 6
unsigned short data[TABLE_X_AXIS_LENGTH * TABLE_Y_AXIS_LENGTH] = {
#include "tables/tablaAitor.h"
		};

unsigned long xAxis[TABLE_X_AXIS_LENGTH] = {
#include "tables/axis/aitorXAxisOri.h"

		};
unsigned int yAxis[TABLE_Y_AXIS_LENGTH] = {
#include "tables/axis/aitorYAxis.h"
		};
//CAN_MCP2515 turboCAN = CAN_MCP2515();
MCP_CAN turboCAN(10);

static const unsigned long MIN_TURBO_RPM_EVENT_PERIOD_MICROS = 1000000; //1000 rpm = 62500 micros / event
static const unsigned long MIN_ENGINE_RPM_EVENT_PERIOD_MICROS = 1000000; //1000 rpm = 62500 micros / event
static const unsigned long RPM_PERIOD_CHECKING_MS = 150;
static const unsigned long RPM_ENGINE_PERIOD_CHECKING_MS = 150;

static const unsigned long MAP_PERIOD_CHECKING_MS = 300;

#define SCREEN_UPDATE_PERIOD 300
#define CAN_COMMAND_READ_PERIOD_MS 300
#define CAN_COMMAND_SEND_PERIOD_MS 15

unsigned long lastCommandSendTime = 0;
unsigned long lastCommandReadTime = 0;

unsigned long screenUpdateTime = 0;

unsigned long lastMAPCheckTime = 0;

unsigned long lastRPMCheckTime = 0;
unsigned long lastRPMEngineCheckTime = 0;

unsigned long lastTurboRPMEvent = 0;
unsigned long previousTurboRPMEvent = 0;
unsigned long lastEngineRPMEvent = 0;
unsigned long previousEngineRPMEvent = 0;

unsigned int engineRPMPulseCounter = 0;
unsigned int turboRPMPulseCounter = 0;

long currentEngineRPM = 0;
long currentTurboRPM = 0;
long maxTurboRPM = 0;

unsigned short currentVanePositionFromCAN = 0;

boolean errorInCAN = false;
/*
 * Turbo Size (cm^2) vs Position
 *   3   4   5   6   7   8   9  10  11  12  13  14
 * 960 918 876 835 793 751 709 667 625 584 542 500
 *  15  16  17  18  19  20  21  22  23  24  25
 * 458 416 375 333 291 249 207 165 124  82  40
 */
#define TURBO_MIN_POS 50
#define TURBO_MAX_POS 940

#define ALARM_OVER_KPA 300

#define MIN_TPS 0
#define MAX_TPS 512

#define TPS_PORT 5
#define MAP_PORT 2
#define MAP_CONVERSION_FACTOR 0.41;

unsigned short currentMode = TABLE_ENGINE;
unsigned short currentView = RT_VIEW_1;
unsigned long lastButtonTime;
unsigned short currentMenuIndex = 0;
unsigned short testDir = 2;

unsigned short getDataValue(int y, int x) {
	return data[TABLE_X_AXIS_LENGTH * (TABLE_Y_AXIS_LENGTH - y - 1) + x];
}

unsigned short lookUpTable(long rpm, unsigned short load) {

	unsigned short lowRPMIndex = 0;
	unsigned short highRPMIndex = TABLE_X_AXIS_LENGTH - 1;
	/* If never set in the loop, low value will equal high value and will be on the edge of the map */
	long lowRPMValue = xAxis[0];
	long highRPMValue = xAxis[TABLE_X_AXIS_LENGTH - 1];

	unsigned char RPMIndex;
	for (RPMIndex = 0; RPMIndex < TABLE_X_AXIS_LENGTH; RPMIndex++) {
		if (xAxis[RPMIndex] < rpm) {
			lowRPMValue = xAxis[RPMIndex];
			lowRPMIndex = RPMIndex;
		} else if (xAxis[RPMIndex] > rpm) {
			highRPMValue = xAxis[RPMIndex];
			highRPMIndex = RPMIndex;
			break;
		} else if (xAxis[RPMIndex] == rpm) {
			lowRPMValue = xAxis[RPMIndex];
			highRPMValue = xAxis[RPMIndex];
			lowRPMIndex = RPMIndex;
			highRPMIndex = RPMIndex;
			break;
		}
	}
	/*Serial.print("xAxis ");
	 Serial.print(lowRPMValue);
	 Serial.print(" ");
	 Serial.println(highRPMValue);*/

	/* Find the bounding cell values and indices for Load */
	unsigned char lowLoadIndex = 0;
	unsigned char highLoadIndex = TABLE_Y_AXIS_LENGTH - 1;
	/* If never set in the loop, low value will equal high value and will be on the edge of the map */
	unsigned short lowLoadValue = yAxis[0];
	unsigned short highLoadValue = yAxis[TABLE_Y_AXIS_LENGTH - 1];

	unsigned char LoadIndex;
	for (LoadIndex = 0; LoadIndex < TABLE_Y_AXIS_LENGTH; LoadIndex++) {
		if (yAxis[LoadIndex] < load) {
			lowLoadValue = yAxis[LoadIndex];
			lowLoadIndex = LoadIndex;
		} else if (yAxis[LoadIndex] > load) {
			highLoadValue = yAxis[LoadIndex];
			highLoadIndex = LoadIndex;
			break;
		} else if (yAxis[LoadIndex] == load) {
			lowLoadValue = yAxis[LoadIndex];
			highLoadValue = yAxis[LoadIndex];
			lowLoadIndex = LoadIndex;
			highLoadIndex = LoadIndex;
			break;
		}
	}

	/* Obtain the four corners surrounding the spot of interest */
	long lowRPMLowLoad =
			data[(TABLE_X_AXIS_LENGTH * lowLoadIndex) + lowRPMIndex];
	long lowRPMHighLoad = data[(TABLE_X_AXIS_LENGTH * highLoadIndex)
			+ lowRPMIndex];
	long highRPMLowLoad = data[(TABLE_X_AXIS_LENGTH * lowLoadIndex)
			+ highRPMIndex];
	long highRPMHighLoad = data[(TABLE_X_AXIS_LENGTH * highLoadIndex)
			+ highRPMIndex];

	/* Find the two side values to interpolate between by interpolation */
	long lowRPMIntLoad = lowRPMLowLoad
			+ ((highLoadValue - lowLoadValue == 0) ?
					0 :
					(((signed long) ((signed long) lowRPMHighLoad
							- lowRPMLowLoad) * (load - lowLoadValue))
							/ (highLoadValue - lowLoadValue)));
	long highRPMIntLoad = highRPMLowLoad
			+ ((highLoadValue - lowLoadValue == 0) ?
					0 :
					(((signed long) ((signed long) highRPMHighLoad
							- highRPMLowLoad) * (load - lowLoadValue))
							/ (highLoadValue - lowLoadValue)));

	/* Interpolate between the two side values and return the result */
	unsigned short returnValue = lowRPMIntLoad
			+ ((highRPMValue - lowRPMValue == 0) ?
					0 :
					((highRPMIntLoad - lowRPMIntLoad) * (rpm - lowRPMValue))
							/ (highRPMValue - lowRPMValue));

	return returnValue;

}

void engineRPMEvent() {
	engineRPMPulseCounter++;
	previousEngineRPMEvent = lastEngineRPMEvent;
	lastEngineRPMEvent = micros(); //overflow cada 70mins aprox.
}

void turboRPMEvent() {
	turboRPMPulseCounter++;
	previousTurboRPMEvent = lastTurboRPMEvent;
	lastTurboRPMEvent = micros();
}

unsigned short engineRPM;
unsigned long turboRPM;
unsigned short tps = 100;
unsigned int mapKPA;
unsigned int maxMAPKpa = 0;
unsigned short rawTPS;
LCDKeypad lcdMenu;
unsigned short j = 40;

char * formatRPM(unsigned long rpm) {
	char * tempChars = "      ";
	if (rpm < 1000) {
		sprintf(tempChars, " %3u", rpm);
	} else {
		sprintf(tempChars, "%3uk", rpm / 1000);
	}
	return tempChars;
}

void printRTData() {
	char * tempChars = "      ";
	lcdMenu.setCursor(0, 0);
	lcdMenu.print("R");
	lcdMenu.setCursor(1, 0);
	lcdMenu.print(formatRPM(currentTurboRPM));
	lcdMenu.setCursor(6, 0);
	lcdMenu.print("V");
	lcdMenu.setCursor(8, 0);
	sprintf(tempChars, "%3u", j);
	lcdMenu.print(tempChars);
	lcdMenu.setCursor(11, 0);
	sprintf(tempChars, "%4u", currentVanePositionFromCAN);
	lcdMenu.print(tempChars);
	lcdMenu.setCursor(15, 0);
	if (errorInCAN) {
		lcdMenu.print("E");
	} else {
		lcdMenu.print("-");
	}

	lcdMenu.setCursor(0, 1);
	lcdMenu.print("MR");
	lcdMenu.print(formatRPM(maxTurboRPM));
	sprintf(tempChars, "%3u", mapKPA);

	lcdMenu.print(" P");
	lcdMenu.print(tempChars);

	lcdMenu.print(" R");
	sprintf(tempChars, "%3u", currentEngineRPM / 100);
	lcdMenu.print(tempChars);
	//lcdMenu.print(tempChars);

}

#define MENU_LENGTH 3
static const String menus[MENU_LENGTH] = { "Modo:     ", "Mapa:      ",
		"Pos fija: " };
static const String modes[5] = { "Off           ", "Modo Mapa T   ",
		"Modo Mapa M   ", "Modo Test     ", "Geom Fija  " };

unsigned int currentFixedPosition = 400;

void printMenu(unsigned short currentMenuIndex) {
	lcdMenu.setCursor(0, 0);
	lcdMenu.print(menus[currentMenuIndex]);
	lcdMenu.setCursor(0, 1);
	switch (currentMenuIndex) {
	case 0:
		lcdMenu.print(modes[currentMode]);
		break;
	case 1:
		lcdMenu.print("Bajo soplado");
		break;
	case 2:
		lcdMenu.print(currentFixedPosition);
		lcdMenu.print("                ");
	}
}

void increaseCurrentMenuIndex(boolean advance) {

	currentMenuIndex += advance ? 1 : -1;
	currentMenuIndex = max(0, min(MENU_LENGTH - 1, currentMenuIndex));
}

void modifySelectedMenu() {
	switch (currentMenuIndex) {
	case 0:
		currentMode++;
		currentMode %= 4;
		break;
	case 1:
		break;
	case 2:
		currentFixedPosition %= 1000;
		currentFixedPosition += 50;
	}
}

void updateScreen() {

	int button = lcdMenu.button();
	unsigned short lastView = currentView;
	if (currentView == MENU_VIEW) {
		if (button != KEYPAD_NONE) {
			lastButtonTime = millis();
			switch (button) {
			case KEYPAD_DOWN:
				increaseCurrentMenuIndex(true);
				break;
			case KEYPAD_UP:
				increaseCurrentMenuIndex(false);
				break;
			case KEYPAD_RIGHT:
				modifySelectedMenu();
				break;
			case KEYPAD_LEFT:
				currentView = RT_VIEW_1;
				break;
			}
		} else if (millis() - lastButtonTime > 5000) {
			currentView = RT_VIEW_1;
		}
	}
	if (currentView == RT_VIEW_1 && button == KEYPAD_SELECT) {
		currentView = MENU_VIEW;
		currentMenuIndex = 0;
		lastButtonTime = millis();
	}
	if (currentView != lastView) {
		lcdMenu.clear();
	}
	switch (currentView) {
	case RT_VIEW_1:
		printRTData();
		break;
	case MENU_VIEW:
		printMenu(currentMenuIndex);
		break;
	}
}

unsigned short getTPSValue() {
	//rawTPS = analogRead(TPS_PORT);
	//return map(rawTPS, MIN_TPS, MAX_TPS, 0, 100);
	tps = 100;
}

unsigned int getMAPValue() {
	return analogRead(MAP_PORT) * MAP_CONVERSION_FACTOR;
}

void sendValue(unsigned short value) {
	if (errorInCAN) {
		turboCAN.begin(CAN_250KBPS); //reset CAN
	}
	byte lo_byte = lowByte(value);
	byte hi_byte = highByte(value);
	byte message_data[8] = { lo_byte, hi_byte, 0x01, 0xFF, 0xFF, 0xFF, 0xFF,
			0xFF };
	byte returnValue = turboCAN.sendMsgBuf(0x0CFFC600, CAN_EXTID, 8,
			message_data);
	if (returnValue != 0) {
		/*	Serial.print("ERROR CAN - ");
		 Serial.println(returnValue);*/
		errorInCAN = true;
	} else {
		errorInCAN = false;
	}

}
//(INT32U id, INT8U ext, INT8U len, INT8U *buf)
//(INT32U *ID, INT8U *len, INT8U buf[])
void readCANValues() {
	//
	byte message_data[8];
	unsigned char length = 0;
	unsigned long int id = 0;
	//0x18FF0A02
	byte value = turboCAN.readMsgBufID(&id, &length, message_data);
	if (length > 0) {
		if (ENABLE_DEBUG) {
			Serial.print("Recibido mensaje con id: ");
			Serial.print(id, HEX);
			Serial.print(" L:");
			Serial.print(length);
			Serial.print(" = ");
			for (int i = 0; i < length; i++) {
				Serial.print(message_data[i], HEX);
				Serial.print(" ");
			}

		}

		if (length > 2 && id == 0x18FFC502) {
			byte lo_byte = message_data[1];
			byte hi_byte = message_data[2];
			currentVanePositionFromCAN = lo_byte + (hi_byte << 8);

		}
		if (ENABLE_DEBUG) {
			Serial.print(" VP: ");
			Serial.print(currentVanePositionFromCAN);
			Serial.println();
		}

	} else {
		//Serial.print("0 leido CAN: ");
		//Serial.println(value);
		currentVanePositionFromCAN = 0;
	}
}

void setup() {
	Serial.begin(115200);
	lcdMenu.begin(16, 2);
	lcdMenu.setCursor(0, 0);
	lcdMenu.print("Holset power!");
	lcdMenu.setCursor(0, 1);
	lcdMenu.print("By AiToR51 ");

	delay(2000);
	lcdMenu.clear();
	lcdMenu.setCursor(0, 0);
	lcdMenu.print("Conectando...");
	delay(1000);
	int fakeCount = 0;
	while (CAN_OK != turboCAN.begin(CAN_250KBPS) && fakeCount < 2) // init can bus : baudrate = 500k
	{
		lcdMenu.setCursor(0, 1);
		lcdMenu.print("Fallo        ");
		delay(500);
		lcdMenu.setCursor(0, 1);
		lcdMenu.print("Reintendando");
		delay(500);
		//fakeCount++;
	}
	lcdMenu.clear();
	lcdMenu.setCursor(0, 0);
	lcdMenu.print("TURBO conectado!");
	if (ENABLE_DEBUG) {
		Serial.println("CAN BUS Shield init ok!");
	}
	delay(1000);

	attachInterrupt(1, turboRPMEvent, FALLING);
	attachInterrupt(0, engineRPMEvent, FALLING);
	lcdMenu.clear();
}

void calcRPM(unsigned long lastRPMEvent, unsigned long previousRPMEvent,
		long * currentRPM, unsigned long minPeriod) {
	noInterrupts();
	if ((micros() - lastRPMEvent) > minPeriod || previousRPMEvent == 0) {
		*currentRPM = 0;
	} else {
		*currentRPM = 60000000 / (lastRPMEvent - previousRPMEvent);
	}
	interrupts();
}

void calcRPM(unsigned long lastRPMEvent, unsigned int * rpmPulseCounter,
		long * currentRPM, unsigned long minPeriod, unsigned long rpmPeriod) {
	noInterrupts();
	if ((micros() - lastRPMEvent) > minPeriod || (*rpmPulseCounter) == 0) {
		*currentRPM = 0;
	} else {
		*currentRPM = (60000 / rpmPeriod) * (*rpmPulseCounter);
	}
	if (ENABLE_DEBUG) {
		Serial.print("calcRPM  ");
		Serial.print(*rpmPulseCounter);
		Serial.print(" ");
		Serial.println(*currentRPM);
	}
	*rpmPulseCounter = 0;
	interrupts();
}

void sendSerialInfo() {
	Serial.print(currentEngineRPM);
	Serial.print(" ");
	Serial.print(currentTurboRPM);
	Serial.print(" ");
	Serial.print(j);
	Serial.print(" ");
	Serial.print(currentVanePositionFromCAN);
	Serial.print(" ");
	Serial.print(mapKPA);
	Serial.print(" ");
	Serial.println(tps);
}

void loop() {
	boolean event = false;
	//lectura RPM turbo
	if (cycleCheck(&lastRPMCheckTime, RPM_PERIOD_CHECKING_MS)) {
		calcRPM(lastTurboRPMEvent, &turboRPMPulseCounter, &currentTurboRPM,
				MIN_TURBO_RPM_EVENT_PERIOD_MICROS, RPM_PERIOD_CHECKING_MS);
		if (currentTurboRPM > maxTurboRPM) {
			maxTurboRPM = currentTurboRPM;
		}
		event = true;
	}
	if (cycleCheck(&lastRPMEngineCheckTime, RPM_ENGINE_PERIOD_CHECKING_MS)) {

		/*calcRPM(lastEngineRPMEvent, &engineRPMPulseCounter, &currentEngineRPM,
		 MIN_ENGINE_RPM_EVENT_PERIOD_MICROS,
		 RPM_ENGINE_PERIOD_CHECKING_MS);*/
		calcRPM(lastEngineRPMEvent, previousEngineRPMEvent, &currentEngineRPM,
				MIN_ENGINE_RPM_EVENT_PERIOD_MICROS);
		//tps = getTPSValue();
		currentEngineRPM = currentEngineRPM * 2;
		event = true;
	}
	if (cycleCheck(&lastMAPCheckTime, MAP_PERIOD_CHECKING_MS)) {
		mapKPA = getMAPValue();
		if (mapKPA > maxMAPKpa) {
			maxMAPKpa = mapKPA;
		}
		event = true;
	}

//lectura RPM motor
//lectura TPS

	if (cycleCheck(&lastCommandSendTime, CAN_COMMAND_SEND_PERIOD_MS)) {

		switch (currentMode) {
		case OFF:
			j = 40;
			break;
		case TABLE_TURBO:
			j = lookUpTable(currentTurboRPM, tps);
			break;
		case TABLE_ENGINE:
			tps = 100;
			//FIXME en novas tablas 100 -nom, 80 - por debajo, 60 por encima, 40 muuuy por encima
			if (currentEngineRPM > 3000) {
				if (currentEngineRPM <= 3500) {
					if (currentTurboRPM > 70000) {
						tps = 40;
					} else if (currentTurboRPM > 65000) {
						tps = 50;
					} else if (currentTurboRPM > 62000) {
						tps = 60;
					} else if (currentTurboRPM < 54000) {
						tps = 80;
					} else if (currentTurboRPM < 58000) {
						tps = 90;
					}
				} else if (currentEngineRPM <= 4000) {
					if (currentTurboRPM > 78000) {
						tps = 40;
					} else if (currentTurboRPM > 74000) {
						tps = 50;
					} else if (currentTurboRPM > 67000) {
						tps = 60;
					} else if (currentTurboRPM < 58000) {
						tps = 80;
					} else if (currentTurboRPM < 63000) {
						tps = 90;
					}

				} else {
					long desiredTurboRPM = currentEngineRPM
							* (16 - ((currentEngineRPM - 4000) * 3) / 3000);
					//4500 * 16 - 7000*13
					/*if (currentEngineRPM <= 4500) {
					 desiredTurboRPM = 78000;
					 } else if (currentEngineRPM <= 5000) {
					 desiredTurboRPM = 80000;
					 } else if (currentEngineRPM <= 5500) {
					 desiredTurboRPM = 82000;
					 } else if (currentEngineRPM <= 6000) {
					 desiredTurboRPM = 84000;
					 } else if (currentEngineRPM <= 6500) {
					 desiredTurboRPM = 85000;
					 }*/
					if (currentTurboRPM > desiredTurboRPM + 4500) {
						tps = 40;
					} else if (currentTurboRPM > desiredTurboRPM + 3500) {
						tps = 50;
					} else if (currentTurboRPM > desiredTurboRPM + 2500) {
						tps = 60;
					} else if (currentTurboRPM < desiredTurboRPM - 5000) {
						tps = 80;
					} else if (currentTurboRPM < desiredTurboRPM - 2800) {
						tps = 90;
					}
				}
			}
			j = lookUpTable(currentEngineRPM, tps);
			break;
		case FIXED_GEOM:
			j = currentFixedPosition;
			break;
		case TEST:
			j += testDir;
			if (j >= 960) {
				testDir = -2;
			} else if (j <= 40) {
				testDir = 2;
			}
			break;
		}
		j = min(960, j);
		sendValue(j);
	}

	if (cycleCheck(&lastCommandReadTime, CAN_COMMAND_READ_PERIOD_MS)) {
		readCANValues();
		event = true;
	}

	if (cycleCheck(&screenUpdateTime, SCREEN_UPDATE_PERIOD)) {
		updateScreen();
		event = true;
	}
	if (event && ENABLE_SERIAL_SENDING_INFO) {
		sendSerialInfo();
	}
	delay(5);

}

